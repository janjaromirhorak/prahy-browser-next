const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    darkMode: 'class',
    content: [
        "./pages/**/*.{js,ts,jsx,tsx}",
        "./components/**/*.{js,ts,jsx,tsx}",
        "./utils/**/*.{js,ts,jsx,tsx}",
    ],
    theme: {
        fontFamily: {
            ...defaultTheme.fontFamily,
            mono: ['Courier Prime', ...defaultTheme.fontFamily.mono]
        },
        extend: {},
    },
    plugins: [],
}
