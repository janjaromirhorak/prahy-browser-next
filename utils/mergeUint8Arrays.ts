const mergeUint8Arrays = (a: Uint8Array, b: Uint8Array): Uint8Array => {
    let mergedArray = new Uint8Array(a.length + b.length);
    mergedArray.set(a, 0);
    mergedArray.set(b, a.length);
    return mergedArray;
}

export default mergeUint8Arrays;