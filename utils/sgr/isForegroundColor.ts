/**
 * Returns true if the SGR code matches a foreground color instruction
 */
const isForegroundColor = (code: number): boolean => {
    return (code >= 30 && code <= 37) || (code >= 90 && code <= 97);
};

export default isForegroundColor;