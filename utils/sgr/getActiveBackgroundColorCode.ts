import isBackgroundColor from "./isBackgroundColor";

/**
 * Returns a SGR code that defines the background color or undefined if there is no such code.
 */
const getActiveBackgroundColorCode = (codes: number[]): number | undefined => {
    return codes.reduce((accumulator: number | undefined, current: number) => {
        if (isBackgroundColor(current)) {
            return current;
        }

        if (current === 0) {
            return undefined;
        }

        return accumulator;
    }, undefined);
};

export default getActiveBackgroundColorCode;