/**
 * Returns true if the SGR code matches a background color instruction
 */
const isBackgroundColor = (code: number): boolean => {
    return (code >= 40 && code <= 47) || (code >= 100 && code <= 107);
};

export default isBackgroundColor;