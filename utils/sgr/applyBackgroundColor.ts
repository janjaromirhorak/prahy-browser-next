import isBackgroundColor from "./isBackgroundColor";
import applySgrCode from "./applySgrCode";

/**
 * Applies a SGR code to an array of codes, only if the specified code is a background code.
 * Passing 0 as code causes a background color reset.
 */
const applyBackgroundColor = (code: number, style: number[]): number[] => {
    if (code === 0) {
        return style.filter(code => !isBackgroundColor(code))
    }

    if (isBackgroundColor(code)) {
        return applySgrCode(code, style);
    }

    return style;
};

export default applyBackgroundColor;