/**
 * Removes a specified SGR code from the array of codes
 */
const removeSgrCode = (code: number, style: number[]):number[] => {
    return style.filter(testedCode => testedCode !== code);
};

export default removeSgrCode;