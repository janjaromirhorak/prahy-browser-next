import hasSgrCode from "./hasSgrCode";
import removeSgrCode from "./removeSgrCode";
import applySgrCode from "./applySgrCode";

/**
 * If the array of codes contains a code, removes this code from the array.
 * Otherwise adds the code to the array.
 */
const toggleSgrCode = (code: number, style: number[]): number[] => {
    if (hasSgrCode(code, style)) {
        return removeSgrCode(code, style);
    } else {
        return applySgrCode(code, style);
    }
};

export default toggleSgrCode;