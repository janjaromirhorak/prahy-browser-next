import isForegroundColor from "./isForegroundColor";
import isBackgroundColor from "./isBackgroundColor";

/**
 * Applies the specified SGR code to an array of SGR codes. Returns an array of SGR codes.
 */
const applySgrCode = (code: number, style: number[]): number[] => {
    if (code === 0) {
        return [0];
    } else {
        let newStyle = style;

        if (isForegroundColor(code)) {
            newStyle = newStyle.filter(code => !isForegroundColor(code));
        } else if (isBackgroundColor(code)) {
            newStyle = newStyle.filter(code => !isBackgroundColor(code));
        }

        return [...new Set<number>([...newStyle, code])];
    }
};

export default applySgrCode;