import isForegroundColor from "./isForegroundColor";

/**
 * Returns a SGR code that defines the foreground color or undefined if there is no such code.
 */
const getActiveForegroundColorCode = (codes: number[]): number | undefined => {
    return codes.reduce((accumulator: number | undefined, current: number) => {
        if (isForegroundColor(current)) {
            return current;
        }

        if (current === 0) {
            return undefined;
        }

        return accumulator;
    }, undefined);
};


export default getActiveForegroundColorCode;