import applySgrCode from "./applySgrCode";

/**
 * Applies all the specified SGR code to an array of SGR codes. Returns an array of SGR codes.
 */
const applySgrCodes = (codes: Iterable<number>, style: number[]): number[] => {
    for (const code of codes) {
        style = applySgrCode(code, style);
    }
    return style;
};

export default applySgrCodes;