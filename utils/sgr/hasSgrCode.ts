/**
 * Returns true only if the array of codes contains the specified SGR code
 */
const hasSgrCode = (code: number, style: number[]): boolean => {
    return style.reduce((accumulator: boolean, current: number) => (
        (accumulator && (current !== 0)) || current === code
    ), false);
};

export default hasSgrCode;