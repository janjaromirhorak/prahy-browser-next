import isForegroundColor from "./isForegroundColor";
import applySgrCode from "./applySgrCode";

/**
 * Applies a SGR code to an array of codes, only if the specified code is a foreground code.
 * Passing 0 as code causes a foreground color reset.
 */
const applyForegroundColor = (code: number, style: number[]): number[] => {
    if (code === 0) {
        return style.filter(code => !isForegroundColor(code))
    }

    if (isForegroundColor(code)) {
        return applySgrCode(code, style);
    }

    return style;
};

export default applyForegroundColor;