/**
 * Send a string over the websocket
 */
const sendText = (text: string, socket: WebSocket) => {
    const blob = new Blob([`${text}\n`], {type: "text/plain"});
    socket.send(blob);
};

export default sendText;