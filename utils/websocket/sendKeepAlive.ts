import {sendBinary} from "./index";

/**
 * Send an empty message over the WebSocket (to keep the connection alive)
 */
const sendKeepAlive = (socket: WebSocket) => {
    sendBinary(new Uint8Array([]), socket);
}

export default sendKeepAlive;