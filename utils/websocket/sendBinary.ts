/**
 * Send a binary message over the websocket
 */
const sendBinary = (payload: Uint8Array, socket: WebSocket) => {
    const blob = new Blob([payload]);
    socket.send(blob);
};

export default sendBinary;