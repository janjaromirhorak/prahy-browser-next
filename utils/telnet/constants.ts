export const T_IAC = 255;
export const T_WILL = 251;
export const T_WONT = 252;
export const T_DO = 253;
export const T_DONT = 254;
export const T_ECHO = 1;
export const T_SUPPRESS_GO_AHEAD = 3;