import TelnetParserNode from "./TelnetParserNode";

export default class TelnetParser {
    state: Array<number>;
    root: TelnetParserNode;
    currentNode: TelnetParserNode;

    constructor() {
        this.state = [];
        this.root = new TelnetParserNode();
        this.currentNode = this.root;
    }

    addCommand(sequence: Array<number>, action: () => void) {
        let currentNode = this.root;
        for (const key of sequence) {
            currentNode = currentNode.getChildCreateIfNotExists(key);
        }
        currentNode.setAction(action);
    }

    traverseToChild(key: number): boolean {
        const child = this.currentNode.getChild(key);
        if (child) {
            this.currentNode = child;
            return true;
        }

        return false;
    }

    currentNodeIsLeaf(): boolean {
        return this.currentNode.isLeaf();
    }

    getCurrentNodeAction(): void | { (): void } {
        return this.currentNode.getAction();
    }

    resetCurrentNode(): void {
        this.currentNode = this.root;
    }
}