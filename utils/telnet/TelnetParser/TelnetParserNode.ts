export default class TelnetParserNode {
    key: void | number;
    action: void | { (): void };
    children: Map<number, TelnetParserNode>;

    constructor(key?: number, action?: () => void) {
        this.key = key;
        this.action = action;

        this.children = new Map();
    }

    isLeaf() {
        return this.children.size === 0;
    }

    setAction(action?: () => void) {
        this.action = action;
    }

    getAction() {
        return this.action;
    }

    hasAction() {
        return this.action !== undefined;
    }

    addChild(key: number, action?: () => void): TelnetParserNode {
        let child = this.children.get(key);
        if (child) {
            child.addChild(key, action);
        } else {
            child = new TelnetParserNode(key, action);
            this.children.set(key, child);
        }
        return child;
    }

    hasChild(key: number) {
        return this.children.has(key);
    }

    getChild(key: number): TelnetParserNode | undefined {
        if (this.hasChild(key)) {
            return this.children.get(key);
        }
    }

    getChildCreateIfNotExists(key: number, action?: () => void): TelnetParserNode {
        let child = this.getChild(key);

        if (!child) {
            child = this.addChild(key, action);
        }

        return child;
    }
}