import "web-streams-polyfill";
// @ts-ignore
import AnsiParser from "node-ansiparser";

import * as SocketUtils from "../../utils/websocket";
import mergeUint8Arrays from "../../utils/mergeUint8Arrays";
import {constants as telnetConstants, TelnetParser} from "../../utils/telnet";
import DisplayData from "../../components/Terminal/DisplayData";
import newSgrTextId from "../../components/Terminal/newSgrTextId";
import {applySgrCodes} from "../../utils/sgr";
import ConnectionStatus from "../../components/Terminal/ConnectionStatus";

const maxRows = 1000;

class MudWorker {
    private readonly socket: WebSocket;
    private textDecoder: TextDecoder;
    private unprocessedData: Uint8Array;
    private stream: TransformStream;
    private streamReader: ReadableStreamReader<Uint8Array>;
    private streamWriter: WritableStreamDefaultWriter<Uint8Array>;

    private ansiParser: AnsiParser;
    private unprocessedDataAnsiParser: AnsiParser;

    private telnetParser: TelnetParser;
    private onDisplayUpdate: { (data: DisplayData[]): void } | undefined;
    private onTemporaryDisplayUpdate: { (data: DisplayData[]): void } | undefined;
    private onHistoryUpdate: {(data: string[]): void} | undefined;
    private onConnectionStatusChange: {(connected: ConnectionStatus): void} | undefined;

    private displayData: DisplayData[] = [];
    private temporaryDisplayData: DisplayData[] = [];
    private sgrState: number[] = [];
    private shouldEchoCommands: boolean = true;
    private commandHistory: string[] = [];

    private firstMessage: boolean = true;

    constructor() {
        this.textDecoder = new TextDecoder();
        this.telnetParser = this.initTelnetParser();

        this.unprocessedData = new Uint8Array(0);

        this.socket = this.initWebSocket();
        this.stream = this.initStream();

        this.streamReader = this.stream.readable.getReader();
        this.streamWriter = this.stream.writable.getWriter();

        this.ansiParser = this.createAnsiParser(false);
        this.unprocessedDataAnsiParser = this.createAnsiParser(true);

        this.initStreamProcessing().then();

        // each 30 seconds send an empty message to keep the connection alive
        setInterval(() => {
            SocketUtils.sendBinary(new Uint8Array(0), this.socket);
        }, 30 * 1000);
    }

    public terminate() {
        self.close();
    }

    private addCommandToHistory(command: string) {
        this.commandHistory = [...this.commandHistory, command];
        if (this.onHistoryUpdate) {
            this.onHistoryUpdate(this.commandHistory);
        }
    }

    public sendCommand(command: string) {

        SocketUtils.sendText(command, this.socket);
        if (this.shouldEchoCommands) {
            this.echoCommand(command);
            this.addCommandToHistory(command);
        }
    }

    public setOnDisplayUpdate(callback: { (data: DisplayData[]): void }) {
        this.onDisplayUpdate = callback;
    }

    public setOnTemporaryDisplayUpdate(callback: { (data: DisplayData[]): void }) {
        this.onTemporaryDisplayUpdate = callback;
    }

    public setOnHistoryUpdate(callback: {(data: string[]): void}) {
        this.onHistoryUpdate = callback;
    }

    public setOnConnectionStatusChange(callback: {(connected: ConnectionStatus): void}) {
        this.onConnectionStatusChange = callback;
    }

    private initTelnetParser(): TelnetParser {
        let telnetParser = new TelnetParser();

        telnetParser.addCommand([telnetConstants.T_IAC, telnetConstants.T_WILL, telnetConstants.T_ECHO], () => {
            SocketUtils.sendBinary(new Uint8Array([telnetConstants.T_IAC, telnetConstants.T_DO, telnetConstants.T_ECHO]), this.socket);
            this.shouldEchoCommands = true;
        });
        telnetParser.addCommand([telnetConstants.T_IAC, telnetConstants.T_DO, telnetConstants.T_ECHO], () => {
            console.log("IAC DO ECHO")
        });
        telnetParser.addCommand([telnetConstants.T_IAC, telnetConstants.T_WONT, telnetConstants.T_ECHO], () => {
            SocketUtils.sendBinary(new Uint8Array([telnetConstants.T_IAC, telnetConstants.T_DONT, telnetConstants.T_ECHO]), this.socket);
            this.shouldEchoCommands = false;
        });
        telnetParser.addCommand([telnetConstants.T_IAC, telnetConstants.T_DONT, telnetConstants.T_ECHO], () => {
            console.log("IAC DONT ECHO")
        });
        telnetParser.addCommand([telnetConstants.T_IAC, telnetConstants.T_WONT, telnetConstants.T_SUPPRESS_GO_AHEAD], () => {
            console.log("IAC WONT SUPPRESS GO AHEAD");
        });

        return telnetParser;
    }

    private echoCommand(command: string) {
        this.flushUnprocessedData();
        this.displayData.push({
            index: newSgrTextId(),
            sgrCodes: [32],
            content: command,
            lineBreak: true
        });
    }

    private async initStreamProcessing(): Promise<void> {
        let result: ReadableStreamDefaultReadResult<Uint8Array>;
        while (result = await this.streamReader.read()) {
            if (result.value) {
                const string = this.textDecoder.decode(result.value.buffer);
                this.ansiParser.parse(string);
            }
        }
    }

    private flushUnprocessedData() {
        const string = this.textDecoder.decode(this.unprocessedData);
        this.ansiParser.parse(string);
        this.unprocessedData = new Uint8Array(0);
        this.temporaryDisplayData = [];
    }

    private initStream(): TransformStream {
        return new TransformStream<Uint8Array, Uint8Array>({
            transform: (chunk, controller) => {
                // append the new chunk to the end of the unprocessed data
                this.unprocessedData = mergeUint8Arrays(this.unprocessedData, chunk);
                this.temporaryDisplayData = [];
                if (this.onTemporaryDisplayUpdate) {
                    this.onTemporaryDisplayUpdate(this.temporaryDisplayData);
                }

                let lastBreak = 0;
                for (let i = 0; i < this.unprocessedData.length; i++) {
                    this.telnetParser.resetCurrentNode();

                    let offset = 0;
                    while (true) {
                        if (this.telnetParser.traverseToChild(this.unprocessedData[i + offset])) {
                            if (this.telnetParser.currentNodeIsLeaf()) {
                                const action = this.telnetParser.getCurrentNodeAction();
                                if (action !== undefined) {
                                    action();
                                }

                                // shift the data to remove this telnet command from the arrayBuffer
                                for (let j = i; j < this.unprocessedData.length - offset - 1; j++) {
                                    this.unprocessedData[j] = this.unprocessedData[j + 3];
                                }

                                // strip last "offset" bytes from the buffer
                                this.unprocessedData = this.unprocessedData.slice(0, this.unprocessedData.length - offset - 1);

                                i--;

                                break;
                            }

                            offset++;
                        } else {
                            break;
                        }
                    }

                    // current character is a line break
                    if (this.unprocessedData[i] === 10) {
                        controller.enqueue(this.unprocessedData.slice(lastBreak, i + 1));
                        lastBreak = i + 1;
                    }
                }

                // if there is some unprocessed data left after the last line break,
                // we process the data as temporary
                if (lastBreak < this.unprocessedData.length) {
                    // process the data after the last data as temporary
                    const string = this.textDecoder.decode(this.unprocessedData.slice(lastBreak, this.unprocessedData.length));
                    this.unprocessedDataAnsiParser.parse(string);

                    // update the array to contain only the data after the last line break
                    this.unprocessedData = this.unprocessedData.slice(lastBreak, this.unprocessedData.length);
                } else {
                    // there is no data left - clear the arrays
                    this.unprocessedData = new Uint8Array(0);
                    this.temporaryDisplayData = [];
                }
            },
            flush: (controller) => {
                controller.enqueue(this.unprocessedData);
                this.unprocessedData = new Uint8Array(0);
            }
        });
    }

    private executeFlag(flag: string): void {
        if (flag === "\n") {
            // if the last element in the display does not contain a line break, add it
            if (this.displayData.length) {
                const last = this.displayData[this.displayData.length - 1];
                if (!last.lineBreak) {
                    this.displayData = [
                        ...this.displayData.slice(0, -1),
                        {
                            ...last,
                            index: newSgrTextId(),
                            lineBreak: true
                        }
                    ];
                    return;
                }
            }

            // otherwise, add a standalone empty text with a line break
            this.displayData = [...this.displayData, {
                index: newSgrTextId(),
                sgrCodes: this.sgrState,
                content: "",
                lineBreak: true
            }];
            return;
        } else if (flag === "\r") {
            // ignore CR
        } else {
            console.log('execute', flag.charCodeAt(0));
        }
    }

    private executeCsi(collected: any, params: number[], flag: string) {
        // http://www.primidi.com/ansi_escape_code/csi_codes
        if (flag === "J") {
            // clear screen
            this.displayData = [];
            this.temporaryDisplayData = [];
        } else if (flag === "H") {
            // move cursor - this does not apply for this implementation
        } else if (flag === "m") {
            // Sets SGR parameters, including text color. After CSI can be zero or more parameters separated with ;.
            // With no parameters, CSI m is treated as CSI 0 m (reset / normal), which is typical of most of the ANSI escape sequences.
            // https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_parameters
            if (params.length === 0) {
                params = [0];
            }

            this.sgrState = applySgrCodes(params, this.sgrState);
        } else {
            console.warn('unknown CSI code:', collected, params, flag)
        }
    }

    private createAnsiParser(buffered: boolean): AnsiParser {
        const onPrint = (text: string) => {
            if (buffered) {
                this.temporaryDisplayData.push({
                    index: newSgrTextId(),
                    sgrCodes: this.sgrState,
                    content: text,
                    lineBreak: false
                });
                if (this.onTemporaryDisplayUpdate) {
                    this.onTemporaryDisplayUpdate(this.temporaryDisplayData);
                }
            } else {
                this.displayData.push({
                    index: newSgrTextId(),
                    sgrCodes: this.sgrState,
                    content: text,
                    lineBreak: false
                });
                this.displayData.slice(-maxRows);
                if (this.onDisplayUpdate) {
                    this.onDisplayUpdate(this.displayData);
                }
            }
        };

        const onExecute = (flag: string) => {
            this.executeFlag(flag);
        }

        const onCSI = (collected: any, params: number[], flag: string) => {
            this.executeCsi(collected, params, flag);
        }

        const terminal = {
            inst_p: onPrint,
            inst_o: function (s: any) {
                console.log('osc', s);
            },
            inst_x: onExecute,
            inst_c: onCSI,
            inst_e: function (collected: any, flag: any) {
                console.log('esc', collected, flag);
            },
            inst_H: function (collected: any, params: any, flag: any) {
                console.log('dcs-Hook', collected, params, flag);
            },
            inst_P: function (dcs: any) {
                console.log('dcs-Put', dcs);
            },
            inst_U: function () {
                console.log('dcs-Unhook');
            }
        };

        return new AnsiParser(terminal);
    }

    /**
     * Convert a Blob to ArrayBuffer
     */
    private static async blobToArrayBuffer(blob: Blob): Promise<ArrayBuffer> {
        if ('arrayBuffer' in blob) return await blob.arrayBuffer();

        // fallback for browsers that do not support blob.arrayBuffer
        return new Promise((resolve, reject) => {
            const reader = new FileReader();
            // @ts-ignore
            reader.onload = () => resolve(reader.result);
            reader.onerror = () => reject;
            reader.readAsArrayBuffer(blob);
        });
    }

    /**
     * Start a WebSocket connection to the bridge and attach the event listeners
     * @private
     */
    private initWebSocket(): WebSocket {
        const address = process.env.NEXT_PUBLIC_WEBSOCKET_ADDRESS;
        if (address) {
            if (this.onConnectionStatusChange) {
                this.onConnectionStatusChange(ConnectionStatus.CONNECTING);
            }
            const socket = new WebSocket(address);

            // assign callbacks

            socket.onopen = () => {
                if (this.onConnectionStatusChange) {
                    this.onConnectionStatusChange(ConnectionStatus.CONNECTED);
                }
            };

            socket.onclose = () => {
                if (this.onConnectionStatusChange) {
                    this.onConnectionStatusChange(ConnectionStatus.DISCONNECTED);
                }
            };

            socket.onerror = () => {
                // todo report
                if (this.onConnectionStatusChange) {
                    this.onConnectionStatusChange(ConnectionStatus.DISCONNECTED);
                }
            };

            socket.onmessage = (event: MessageEvent) => {
                if (this.firstMessage) {
                    this.firstMessage = false;
                    // automatically choose utf-8
                    SocketUtils.sendText("u", socket);
                } else {
                    MudWorker.blobToArrayBuffer(event.data)
                        .then(buffer => {
                            // process the incoming message
                            return this.streamWriter.write(new Uint8Array(buffer));
                        })
                }
            };

            return socket;
        }

        // todo throw a better exception
        throw new Error();
    }
}

export default MudWorker;