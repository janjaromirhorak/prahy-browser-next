import React from "react";
import type {NextPage} from 'next'
import Head from 'next/head'
import dynamic from "next/dynamic";

// we do not want to render the terminal using SSR, because of the underlying websocket connection
const Terminal = dynamic(
    () => import('../components/Terminal'),
    { ssr: false }
)

const Home: NextPage = () => {
    return (
        <>
            <Head>
                <title>Prahy</title>
                <meta name="description" content="Vítej mezi Prahy! Kdesi v tomto domě se nachází první Práh. Jakmile jej překročíš, vstoupíš do světa dost velkého na to, aby bylo nad tvé síly poznat jej celý."/>
            </Head>

            <div className={"h-screen bg-white bg-black text-black text-white"}>
                <Terminal/>
            </div>
        </>
    )
};

export default Home
