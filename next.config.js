const commonHeaders = [
    {
        key: 'X-DNS-Prefetch-Control',
        value: 'on'
    },
    {
        key: 'X-Content-Type-Options',
        value: 'nosniff'
    },
    {
        key: 'X-XSS-Protection',
        value: '1; mode=block'
    },
    {
        key: 'X-Frame-Options',
        value: 'DENY'
    },
    {
        key: 'Referrer-Policy',
        value: 'no-referrer'
    },
    {
        key: 'Permissions-Policy',
        value: 'accelerometer=(), ambient-light-sensor=(), autoplay=(), battery=(), camera=(), cross-origin-isolated=(), display-capture=(), document-domain=(), encrypted-media=(), execution-while-not-rendered=(), execution-while-out-of-viewport=(), fullscreen=*, geolocation=(), gyroscope=(), keyboard-map=*, magnetometer=(), microphone=(), midi=(), navigation-override=(), payment=(), picture-in-picture=(), publickey-credentials-get=(), screen-wake-lock=(), sync-xhr=(), usb=(), web-share=*, xr-spatial-tracking=()'
    },
    {
        key: "Access-Control-Allow-Origin",
        value: "null"
    }
];

if (process.env.NODE_ENV === "production") {
    commonHeaders.push({
        key: 'Content-Security-Policy',
        value: `frame-ancestors 'self'; default-src 'self'; script-src 'self'; style-src 'self' 'unsafe-inline'; img-src 'self'; connect-src 'self' ${process.env.NEXT_PUBLIC_WEBSOCKET_ADDRESS}; media-src 'self'; frame-src 'self'; base-uri 'none'`
    });
}

if (process.env.NEXT_PUBLIC_WEBSOCKET_ADDRESS) {
    const url = new URL(process.env.NEXT_PUBLIC_WEBSOCKET_ADDRESS);
    url.protocol = "https";
    url.port = "";
    commonHeaders.push({
        key: 'Link',
        value: `<${url.toString()}>; rel=dns-prefetch`
    })
}

/** @type {import('next').NextConfig} */
module.exports = {
    reactStrictMode: true,
    async headers() {
        return [
            {
                source: '/:path*',
                headers: commonHeaders
            }
        ]
    }
}
