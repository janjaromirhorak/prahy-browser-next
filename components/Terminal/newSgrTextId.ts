let lastId: number = 0;

const newSgrTextId = (): number => {
    lastId++;
    return lastId;
};

export default newSgrTextId;