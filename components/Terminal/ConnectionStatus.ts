enum ConnectionStatus {
    DISCONNECTED,
    CONNECTING,
    CONNECTED
}

export default ConnectionStatus;