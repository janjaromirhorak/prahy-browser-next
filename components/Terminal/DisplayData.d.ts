interface DisplayData {
    index: number,
    sgrCodes: number[],
    content: string,
    lineBreak?: boolean
}

export default DisplayData;