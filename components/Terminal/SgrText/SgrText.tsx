import React, {PropsWithChildren, useMemo} from "react";
import tailwindColors from "./tailwindColors";
import colorMap from "./colorMap";

interface Props {
    sgrCodes?: number[]
}

const SgrText = ({children, sgrCodes = [0]}: PropsWithChildren<Props>) => {
    const classNames: string[] = useMemo(() => {
        const codeSet = new Set<number>(sgrCodes?.length ? sgrCodes : [0]);
        let classNames = ["leading-[inherit]"];

        // reset or normal
        if (codeSet.has(0)) {
            classNames.push(tailwindColors.text.brightWhite);
            classNames.push(tailwindColors.bg.black);
        }

        // 0 + invert
        if (codeSet.has(7) && codeSet.has(0)) {
            classNames.push(tailwindColors.text.black);
            classNames.push(tailwindColors.bg.brightWhite);
        }

        // bold or increased intensity
        if (codeSet.has(1)) {
            classNames.push("font-bold");
        }

        // faint, decreased intensity or dim
        if (codeSet.has(2)) {
            classNames.push("opacity-75");
        }

        // italic
        if (codeSet.has(3)) {
            classNames.push("italic");
        }

        // underline
        if (codeSet.has(4)) {
            classNames.push("underline");
            classNames.push("underline-offset-2");
        }

        // slow blink
        if (codeSet.has(5)) {
            classNames.push("animate-[pulse_1s_linear_infinite]");
        }

        // rapid blink
        if (codeSet.has(6)) {
            classNames.push("animate-[pulse_500ms_linear_infinite]");
        }

        // crossed out
        if (codeSet.has(9)) {
            classNames.push("line-through");
        }

        // normal intensity
        if (codeSet.has(22)) {
            classNames.push("opacity-100");
            classNames.push("font-normal");
        }

        // neither italic, nor blackletter
        if (codeSet.has(23)) {
            classNames.push("non-italic");
        }

        // not underlined
        if (codeSet.has(24)) {
            classNames.push("no-underline");
        }

        // not blinking
        if (codeSet.has(25)) {
            classNames.push("animate-none");
        }

        // not crossed out
        if (codeSet.has(29)) {
            classNames.push("no-underline");
        }

        const textColorClass = (color: string): string => {
            // @ts-ignore
            return tailwindColors.text[color] ?? "";
        }

        const bgColorClass = (color: string): string => {
            // @ts-ignore
            return tailwindColors.bg[color] ?? "";
        }

        for (const key of Object.keys(colorMap)) {
            const colorName = colorMap[key as keyof typeof colorMap];
            const brightColorName = "bright" + colorName.charAt(0).toUpperCase() + colorName.slice(1);

            const textKey = Number(key);
            const bgKey = textKey + 10;
            const brightTextKey = textKey + 60;
            const brightBgKey = textKey + 70;

            // colors are not inverted
            if (!codeSet.has(7)) {
                // foreground color
                if (codeSet.has(textKey)) {
                    // @ts-ignore
                    classNames.push(textColorClass(colorName));
                }

                // background color
                if (codeSet.has(bgKey)) {
                    classNames.push(bgColorClass(colorName));
                }

                // bright foreground color
                if (
                    codeSet.has(brightTextKey)
                    || (codeSet.has(textKey) && codeSet.has(1))
                ) {
                    classNames.push(textColorClass(brightColorName));
                }

                // bright background color
                if (codeSet.has(brightBgKey)) {
                    classNames.push(bgColorClass(brightColorName));
                }
            } else {
                // inverted colors

                // foreground color
                if (codeSet.has(textKey)) {
                    // @ts-ignore
                    classNames.push(bgColorClass(colorName));
                }

                // background color
                if (codeSet.has(bgKey)) {
                    classNames.push(textColorClass(colorName));
                }

                // bright foreground color
                if (
                    codeSet.has(brightTextKey)
                    || (codeSet.has(textKey) && codeSet.has(1))
                ) {
                    classNames.push(bgColorClass(brightColorName));
                }

                // bright background color
                if (codeSet.has(brightBgKey)) {
                    classNames.push(textColorClass(brightColorName));
                }
            }
        }

        return classNames;
    }, [sgrCodes]);

    return (
        <span className={classNames.join(" ")}>
            {children}
        </span>
    )
};

export default SgrText;