const tailwindColors = {
    "text": {
        "black": "text-black",
        "red": "text-red-500",
        "green": "text-green-500",
        "yellow": "text-amber-600",
        "blue": "text-blue-700",
        "magenta": "text-fuchsia-600",
        "cyan": "text-cyan-500",
        "white": "text-gray-400",
        "brightBlack": "text-gray-700",
        "brightRed": "text-red-400",
        "brightGreen": "text-lime-300",
        "brightYellow": "text-yellow-300",
        "brightBlue": "text-blue-500",
        "brightMagenta": "text-fuchsia-400",
        "brightCyan": "text-cyan-300",
        "brightWhite": "text-white"
    },
    "bg": {
        "black": "bg-black",
        "red": "bg-red-500",
        "green": "bg-green-500",
        "yellow": "bg-amber-600",
        "blue": "bg-blue-700",
        "magenta": "bg-fuchsia-600",
        "cyan": "bg-cyan-500",
        "white": "bg-gray-400",
        "brightBlack": "bg-gray-700",
        "brightRed": "bg-red-400",
        "brightGreen": "bg-lime-300",
        "brightYellow": "bg-yellow-300",
        "brightBlue": "bg-blue-500",
        "brightMagenta": "bg-fuchsia-400",
        "brightCyan": "bg-cyan-300",
        "brightWhite": "bg-white"
    }
};

export default tailwindColors;



























