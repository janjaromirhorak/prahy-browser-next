import React, {ChangeEvent, FormEventHandler, useCallback, useEffect, useRef, useState} from "react";
import { ArrowRightIcon, RefreshIcon } from "@heroicons/react/solid";
import ScrollableFeed from 'react-scrollable-feed';

// @ts-ignore
import AnsiParser from "node-ansiparser";

import * as Comlink from "comlink";
import type {default as MudWorkerType} from "../../workers/MudWorker";

import SgrText from "./SgrText";
import DisplayData from "./DisplayData";
import ConnectionStatus from "./ConnectionStatus";
import MudWorker from "../../workers/MudWorker";

const Terminal = () => {
    const [display, setDisplay] = useState<DisplayData[]>([]);
    const [bufferedDisplay, setBufferedDisplay] = useState<DisplayData[]>([]);

    const [input, setInput] = useState<string>("");
    const [connectionStatus, setConnectionStatus] = useState<ConnectionStatus>(ConnectionStatus.DISCONNECTED);

    const mudWorkerRef = useRef<MudWorkerType | undefined>(undefined);

    const onInputChange = useCallback((e: ChangeEvent<HTMLInputElement>) => {
        setInput(e.currentTarget.value);
    }, []);

    const initMudWorker = useCallback(async () => {
        if (process.env.NEXT_PUBLIC_USE_WEB_WORKER) {
            const MudWorker = Comlink.wrap<MudWorkerType>(
                new Worker(new URL('../../workers/mud.worker.ts', import.meta.url))
            );

            // @ts-ignore
            mudWorkerRef.current = await new MudWorker();

            await Promise.all([
                mudWorkerRef.current?.setOnDisplayUpdate(Comlink.proxy(setDisplay)),
                mudWorkerRef.current?.setOnTemporaryDisplayUpdate(Comlink.proxy(setBufferedDisplay)),
                // mudWorkerRef.current?.setOnHistoryUpdate(Comlink.proxy(setHistory)),
                mudWorkerRef.current?.setOnConnectionStatusChange(Comlink.proxy(setConnectionStatus))
            ]);
        } else {
            mudWorkerRef.current = new MudWorker();
            mudWorkerRef.current?.setOnDisplayUpdate(setDisplay);
            mudWorkerRef.current?.setOnTemporaryDisplayUpdate(setBufferedDisplay);
            // mudWorkerRef.current?.setOnHistoryUpdate(setHistory);
            mudWorkerRef.current?.setOnConnectionStatusChange(setConnectionStatus);
        }
    }, []);

    const replaceMudWorker = useCallback(async () => {
        if (process.env.NEXT_PUBLIC_USE_WEB_WORKER) {
            mudWorkerRef.current?.terminate();
        }
        await initMudWorker();
    }, [initMudWorker]);

    const onSubmit: FormEventHandler = useCallback((e) => {
        e.preventDefault();
        if (connectionStatus === ConnectionStatus.CONNECTED) {
            mudWorkerRef.current?.sendCommand(input);
            setInput("");
        } else if (connectionStatus === ConnectionStatus.DISCONNECTED) {
            setDisplay([]);
            setBufferedDisplay([]);
            replaceMudWorker().then();
        }
    }, [connectionStatus, input, replaceMudWorker]);

    useEffect(() => {
        initMudWorker().then();

        return () => {
            if (process.env.NEXT_PUBLIC_USE_WEB_WORKER) {
                mudWorkerRef.current?.terminate();
            }
        }
    }, [initMudWorker]);

    return (
        <div className="mx-auto w-[50rem] max-w-full h-screen flex flex-col font-mono gap-y-2 p-4">
            <ScrollableFeed className="flex-1 h-full overflow-y-scroll whitespace-pre text-[1.95vw] md:text-base leading-tight md:leading-tight">
                {display.map(({index, sgrCodes, content, lineBreak}) => (
                    <SgrText
                        key={index}
                        sgrCodes={sgrCodes}
                    >{content}{lineBreak ? <br/> : null}</SgrText>
                ))}
                <br/>
                {bufferedDisplay.map(({index, sgrCodes, content, lineBreak}) => (
                    <SgrText
                        key={index}
                        sgrCodes={sgrCodes}
                    >{content}{lineBreak ? <br/> : null}</SgrText>
                ))}
            </ScrollableFeed>
            <form className="flex-1 grow-0 pt-2 flex flex-row gap-4 justify-center" onSubmit={onSubmit}>
                {(connectionStatus === ConnectionStatus.CONNECTED) ? (
                    <>
                        <input onChange={onInputChange} value={input} autoFocus={true}
                               className={`
                                    transition-all bg-zinc-900/50 text-white
                                    outline outline-offset-0 outline-zinc-600/50
                                    focus:outline-zinc-600/75 rounded-md w-full text-lg font-mono p-2
                               `}
                        />
                        <button className={`
                            transition text-white/80 hover:text-white active:text-white focus:text-white
                            rounded-full
                            flex-none flex justify-center h-12 w-12 bg-zinc-600/50 hover:bg-zinc-600/75
                        `}
                        >
                            <ArrowRightIcon className="w-6"/>
                        </button>
                    </>
                ) : (
                    <button className={`
                            transition text-white/80 hover:text-white active:text-white focus:text-white
                            rounded-full
                            flex justify-center w-12 h-12 bg-zinc-900/70 hover:bg-zinc-600/50
                            disabled:animate-[spin_3s_linear_infinite_reverse]
                        `}
                            disabled={connectionStatus === ConnectionStatus.CONNECTING}
                    >
                        <RefreshIcon className="w-6"/>
                    </button>
                )}
            </form>
        </div>
    );
};

export default Terminal;